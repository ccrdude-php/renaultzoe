<?php

/**
 * Gigya API class testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\RenaultZoe;

require_once __DIR__ . '/../source/load.php';
require_once __DIR__ . '/phpunit-10.phar';

use PHPUnit\Framework\TestCase;

/**
 * Gigya API class testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
final class GigyaTest extends TestCase
{
    /**
     * Passes invalid credentials and checks if exception is thrown.
     *
     * @return void
     */
    public function testLoginFail(): void
    {
        $storage = new ConfigFileStorage(__DIR__ . '/config.json');
        $cfg = new Config($storage);
        $cfg->setUsername('adalbert@example.com');
        $cfg->setPassword('dieRüstungUntermAdelskleid');
        $g = new Gigya($cfg);
        $this->expectException(\RioGrande\RenaultZoe\GigyaException::class);
        $this->assertFalse($g->login(), 'login');
    }
}
