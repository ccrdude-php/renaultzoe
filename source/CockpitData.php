<?php

/**
 * Data class for cockpit data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for cockpit data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 *
 * {
 *     "data": {
 *         "type": "Car",
 *         "id": "VF1AG000269300982",
 *         "attributes": {
 *             "fuelQuantity": 0,
 *             "fuelAutonomy": 0,
 *             "totalMileage": 22665
 *         }
 *     }
 * }
 */
class CockpitData extends CustomData
{
    /**
     * Returns the total mileage of the vehicle.
     *
     * @return int
     */
    public function getTotalMileage(): int
    {
        $this->validateDataExists('totalMileage');
        return $this->FData['data']['attributes']['totalMileage'];
    }
}
