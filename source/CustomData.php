<?php

/**
 * Data class for Custom data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for Custom data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class CustomData
{
    protected array $FData;
    protected ?Query $FQuery;

    /**
     * Constructs a data class for handling Custom data.
     *
     * @param array      $AQueryData The data returned by the query.
     * @param Query|null $TheQuery   The query used to retrieved the data.
     */
    public function __construct(array $AQueryData, ?Query $TheQuery)
    {
        $this->FData = $AQueryData;
        $this->FQuery = $TheQuery;
    }

    /**
     * Returns the data as a printed array.
     *
     * @return string
     */
    public function dumpData(): string
    {
        return print_r($this->FData, true);
    }

    /**
     * Returns the data as formatted JSON.
     *
     * @return string
     */
    public function dumpDataJSON(): string
    {
        return json_encode($this->FData, JSON_PRETTY_PRINT);
    }

    /**
     * Validates if the vehicle data includes the expected fields.
     *
     * @param string $TheField Field that is expected.
     *
     * @return void
     */
    protected function validateDataExists(
        string $TheField
    ): void {
        if (!isset($this->FData['data'])) {
            throw new KamereonException(
                'Missing data field',
                $this->FQuery,
                'data'
            );
        }
        if (!isset($this->FData['data']['attributes'])) {
            throw new KamereonException(
                'Missing data.attributes field',
                $this->FQuery,
                'data.attributes'
            );
        }
        if (!isset($this->FData['data']['attributes'][$TheField])) {
            throw new KamereonException(
                'Missing data.attributes.' . $TheField,
                $this->FQuery,
                "data.attributes." . $TheField
            );
        }
    }
}
