<?php

/**
 * Data class for ChargingSettings data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for ChargingSettings data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 *
 * {
 *     "data": {
 *         "type": "Car",
 *         "id": "VF1AG000269300982",
 *         "attributes": {
 *             "dateTime": "2023-04-26T21:02:38.265151Z",
 *             "mode": "always",
 *             "schedules": [
 *                 {
 *                     "id": 1,
 *                     "activated": false
 *                 },
 *                 {
 *                     "id": 2,
 *                     "activated": false
 *                 },
 *                 {
 *                     "id": 3,
 *                     "activated": false
 *                 },
 *                 {
 *                     "id": 4,
 *                     "activated": false
 *                 },
 *                 {
 *                     "id": 5,
 *                     "activated": false
 *                 }
 *             ]
 *         }
 *     }
 * }
 */
class ChargingSettingsData extends CustomData
{
}
