<?php

/**
 * Specific exception for the Zoe API.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Specific exception for the Zoe API.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class ZoeException extends \Exception
{
    protected ?Query $FQuery;
    protected ?string $FDataField;

    /**
     * Creates a new exception.
     *
     * @param [type]      $TheMessage   Message to be displayed.
     * @param Query|null  $TheQuery     Query that caused the exception.
     * @param string|null $TheDataField Data field that caused the exception.
     */
    public function __construct($TheMessage, ?Query $TheQuery, ?string $TheDataField)
    {
        parent::__construct($TheMessage);
        $this->FQuery = $TheQuery;
        $this->FDataField = $TheDataField;
    }

    /**
     * Returns the data field that caused the exception, if applicable.
     *
     * @return string|null
     */
    public function getDataField(): ?string
    {
        return $this->FDataField;
    }

    /**
     * Returns an error code if the answer has errors.
     *
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        if (is_null($this->FQuery)) {
            return null;
        }
        $a = $this->FQuery->getDebugOutput();
        if (!isset($a['incoming'])) {
            return null;
        }
        if (!isset($a['incoming']['body'])) {
            return null;
        }
        if (!isset($a['incoming']['body']['errorcode'])) {
            return null;
        }
        return intval($a['incoming']['body']['errorcode']);
    }

    /**
     * Returns the query leading to the exception, if available.
     * Null otherwise.
     *
     * @return Query|null
     */
    public function getQuery(): ?Query
    {
        return $this->FQuery;
    }
}
