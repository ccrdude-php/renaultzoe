<?php

/**
 * Data class for BatteryStatus data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for BatteryStatus data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 *
 * {
 *     "data": {
 *         "type": "Car",
 *         "id": "VF1AG000269300982",
 *         "attributes": {
 *             "rangeHvacOn": 0,
 *             "chargeStatus": 0,
 *             "batteryLevel": 78,
 *             "plugStatus": 0
 *         }
 *     }
 * }
 */
class BatteryStatusData extends CustomData
{
    /**
     * Returns the battery level of the vehicle.
     *
     * @return int
     */
    public function getBatteryLevel(): int
    {
        $this->validateDataExists('batteryLevel');
        return $this->FData['data']['attributes']['batteryLevel'];
    }
}
