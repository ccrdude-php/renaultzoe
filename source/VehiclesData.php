<?php

/**
 * Manages the data returned by a query for vehicles from the Kamereon API.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Manages the data returned by a query for vehicles from the Kamereon API.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class VehiclesData
{
    protected array $FData;
    protected ?Query $FQuery;

    /**
     * Constructs a data class for handling vehicles data.
     *
     * @param array      $AQueryData The data returned by the query.
     * @param Query|null $TheQuery   The query used to retrieved the data.
     */
    public function __construct(array $AQueryData, ?Query $TheQuery)
    {
        $this->FData = $AQueryData;
        $this->FQuery = $TheQuery;
    }

    /**
     * Returns the number of vehicles.
     *
     * @return int
     */
    public function getVehicleCount(): int
    {
        if (!isset($this->FData['vehicleLinks'])) {
            throw new KamereonException(
                'Missing vehicleLinks field',
                $this->FQuery,
                'vehicleLinks'
            );
        }
        return count($this->FData['vehicleLinks']);
    }

    /**
     * Returns the VIN of a specific vehicle.
     *
     * @param int $TheIndex Index of the vehicle.
     *
     * @return string
     */
    public function getVehicleVIN(int $TheIndex): string
    {
        $this->validateVehicleLinksDataExists($TheIndex, 'vin');
        return $this->FData['vehicleLinks'][$TheIndex]['vin'];
    }

    /**
     * Returns the brand of a specific vehicle.
     *
     * @param int $TheIndex Index of the vehicle.
     *
     * @return string
     */
    public function getVehicleBrand(int $TheIndex): string
    {
        $this->validateVehicleLinksDataExists($TheIndex, 'brand');
        return $this->FData['vehicleLinks'][$TheIndex]['brand'];
    }

    /**
     * Returns the status of a specific vehicle.
     *
     * @param int $TheIndex Index of the vehicle.
     *
     * @return string
     */
    public function getVehicleStatus(int $TheIndex): string
    {
        $this->validateVehicleLinksDataExists($TheIndex, 'status');
        return $this->FData['vehicleLinks'][$TheIndex]['status'];
    }

    /**
     * Returns a plain text table of vehicles stored within this object.
     *
     * @return string
     */
    public function getTableAsPlainText(): string
    {
        $ret = " #  VIN                Brand         Status      ...\n";
        $ret .= "-----------------------------------------------------\n";
        $iCount = $this->getVehicleCount();
        for ($i = 0; $i < $iCount; $i++) {
            $ret .= sprintf("%2d  ", $i);
            $ret .= sprintf("%17s  ", $this->getVehicleVIN($i));
            $ret .= sprintf("%-12s  ", $this->getVehicleBrand($i));
            $ret .= sprintf("%-10s  ", $this->getVehicleStatus($i));
            $ret .= "...\n";
        }
        return $ret;
    }

    /**
     * Validates if the vehicle data includes the expected fields.
     *
     * @param int    $TheIndex Index of the vehicle.
     * @param string $TheField Field that is expected.
     *
     * @return void
     */
    protected function validateVehicleLinksDataExists(
        int $TheIndex,
        string $TheField
    ): void {
        if (!isset($this->FData['vehicleLinks'])) {
            throw new KamereonException(
                'Missing vehicleLinks field',
                $this->FQuery,
                'vehicleLinks'
            );
        }
        $iCount = count($this->FData['vehicleLinks']);
        if (($TheIndex < 0) || ($TheIndex >= $iCount)) {
            throw new KamereonException(
                "Index of vehicle {$TheIndex} is out of range ({$iCount} total)",
                $this->FQuery,
                'vehicleLinks'
            );
        }
        if (!isset($this->FData['vehicleLinks'][$TheIndex])) {
            throw new KamereonException(
                'Missing vehicleLinks vehicle array item',
                $this->FQuery,
                "vehicleLinks[{$TheIndex}]"
            );
        }
        if (!isset($this->FData['vehicleLinks'][$TheIndex][$TheField])) {
            throw new KamereonException(
                'Missing vehicleLinks vehicle ' . $TheField,
                $this->FQuery,
                "vehicleLinks[{$TheIndex}]." . $TheField
            );
        }
    }
}
