<?php

/**
 * Data class for Location data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for Location data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 *
 * {
 *     "data": {
 *         "type": "Car",
 *         "id": "VF1AG000269300982",
 *         "attributes": {
 *             "gpsDirection": null,
 *             "gpsLatitude": 51.6377947222222,
 *             "gpsLongitude": 8.52241277777778,
 *             "lastUpdateTime": "2023-04-26T16:27:50Z"
 *         }
 *     }
 * }
 */
class LocationData extends CustomData
{
    /**
     * Returns the total mileage of the vehicle.
     *
     * @return int
     */
    public function getLatitude(): float
    {
        $this->validateDataExists('gpsLatitude');
        return $this->FData['data']['attributes']['gpsLatitude'];
    }

    /**
     * Returns the total mileage of the vehicle.
     *
     * @return int
     */
    public function getLongitude(): float
    {
        $this->validateDataExists('gpsLongitude');
        return $this->FData['data']['attributes']['gpsLongitude'];
    }
}
