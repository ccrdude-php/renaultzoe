<?php

/**
 * Loads all required files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

require_once __DIR__ . '/Query.php';
require_once __DIR__ . '/ZoeException.php';
require_once __DIR__ . '/GigyaException.php';
require_once __DIR__ . '/KamereonException.php';
require_once __DIR__ . '/ConfigStorage.php';
require_once __DIR__ . '/ConfigFileStorage.php';
require_once __DIR__ . '/Zoe.php';
require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/Gigya.php';
require_once __DIR__ . '/Kamereon.php';
require_once __DIR__ . '/CustomData.php';
require_once __DIR__ . '/VehiclesData.php';
require_once __DIR__ . '/BatteryStatusData.php';
require_once __DIR__ . '/ChargeModeData.php';
require_once __DIR__ . '/ChargingSettingsData.php';
require_once __DIR__ . '/CockpitData.php';
require_once __DIR__ . '/LocationData.php';
require_once __DIR__ . '/LockStatusData.php';
require_once __DIR__ . '/LogWriter.php';
