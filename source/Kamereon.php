<?php

/**
 * Interface to the Kamereon API used by the Renault Zoe interface.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @see        https://renault-api.readthedocs.io/en/latest/endpoints.html
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Interface to the Kamereon API used by the Renault Zoe interface.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class Kamereon
{
    protected Config $FConfig;
    protected string $FCURLError = '';

    /**
     * Constructs a Kamereon API handler.
     *
     * @param Config $TheConfig The configuration to be used.
     */
    public function __construct(Config $TheConfig)
    {
        $this->FConfig = $TheConfig;
    }

    /**
     * Checks if a VIN is set, and if not, tries to query vehicle data and
     * retrieve the VIN of the first vehicle.
     *
     * @return bool
     */
    protected function ensureDefaultVIN(): bool
    {
        if (strlen($this->FConfig->getVIN()) > 0) {
            return true;
        }
        $aVehicles = $this->getVehiclesData();
        if (is_null($aVehicles)) {
            return false;
        }
        if ($aVehicles->getVehicleCount() == 0) {
            return false;
        }
        $this->FConfig->setVIN($aVehicles->getVehicleVIN(0));
        return true;
    }

    /**
     * Returns the Account ID further required, requires a valid API key
     * as well as an ID token from the Gigya API.
     *
     * @return array|null
     */
    public function getAccountID(): ?array
    {
        $aHeaders = array(
            'apikey: ' . $this->FConfig->getKamereonAPIKey(),
            'x-gigya-id_token: ' . $this->FConfig->getIDToken(),
        );
        $sURL = $this->FConfig->getKamereonBaseURL();
        $sURL .= 'commerce/v1/persons/';
        $sURL .= $this->FConfig->getPersonID();
        $sURL .= '?country=';
        $sURL .= $this->FConfig->getCountry();
        $q = new Query($sURL);
        $q->setHeaders($aHeaders);
        $res = $q->execute();
        if (is_null($res)) {
            $this->FCURLError = $q->getError();
            $ret = null;
        } else {
            $this->FConfig->setAccountID($res['accounts'][0]['accountId']);
            $ret = $res;
        }
        return $ret;
    }

    /**
     * Handles a regular data query for a specified vehicle.
     *
     * @param string      $ADataType     The type as used as the filename in the
     *                                   query.
     * @param string|null $TheVIN        The Vehicle Identification Number of the
     *                                   vehicle to query.
     * @param Query|null  $TheQuery      Returns the query (for use in debugging).
     * @param int         $TheAPIVersion Version for this specific cll
     *
     * @return array|null
     */
    protected function getData(
        string $ADataType,
        ?string $TheVIN,
        ?Query &$TheQuery,
        int $TheAPIVersion = 1
    ): ?array {
        $TheQuery = null;
        $aHeaders = array(
            'apikey: ' . $this->FConfig->getKamereonAPIKey(),
            'x-gigya-id_token: ' . $this->FConfig->getIDToken(),
        );
        try {
            if (is_null($TheVIN)) {
                $this->ensureDefaultVIN();
                $TheVIN = $this->FConfig->getVIN();
            }
            $sURL = $this->FConfig->getKamereonBaseURL();
            $sURL .= 'commerce/v1/accounts/';
            $sURL .= $this->FConfig->getAccountID();
            $sURL .= "/kamereon/kca/car-adapter/v{$TheAPIVersion}/cars/";
            $sURL .= $TheVIN;
            $sURL .= '/' . $ADataType;
            $sURL .= '?country=';
            $sURL .=  $this->FConfig->getCountry();
            $TheQuery = new Query($sURL);
            $TheQuery->setHeaders($aHeaders);
            $res = $TheQuery->execute();
            if (is_null($res)) {
                $this->FCURLError = $TheQuery->getError();
                $ret = null;
            } else {
                $ret = $res;
            }
        } catch (\Exception $e) {
            throw new ZoeException($e->getMessage(), $TheQuery, null);
        }
        return $ret;
    }

    /**
     * Retrieves the battery status data.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return BatteryStatusData|null
     */
    public function getBatteryStatusData(?string $TheVIN): ?BatteryStatusData
    {
        $res = $this->getData('battery-status', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new BatteryStatusData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves the charge mode data.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return ChargeModeData|null
     */
    public function getChargeModeData(?string $TheVIN): ?ChargeModeData
    {
        $res = $this->getData('charge-mode', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new ChargeModeData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves the charge mode data.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return ChargingSettingsData|null
     */
    public function getChargingSettingsData(?string $TheVIN): ?ChargingSettingsData
    {
        $res = $this->getData('charging-settings', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new ChargingSettingsData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves the cockpit data, which is the source for getting the
     * total mileage of the vehicle.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return CockpitData|null
     */
    public function getCockpitData(?string $TheVIN): ?CockpitData
    {
        $res = $this->getData('cockpit', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new CockpitData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves the location data.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return LocationData|null
     */
    public function getLocationData(?string $TheVIN): ?LocationData
    {
        $res = $this->getData('location', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new LocationData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves the lock status data, which is the source for getting the
     * total mileage of the vehicle.
     *
     * @param string|null $TheVIN The Vehicle Identification Number of the
     *                            vehicle to query.
     *
     * @return LockStatusData|null
     */
    public function getLockStatusData(?string $TheVIN): ?LockStatusData
    {
        $res = $this->getData('lock-status', $TheVIN, $q);
        if (!is_null($res)) {
            $ret = new LockStatusData($res, $q);
            return $ret;
        }
        return null;
    }

    /**
     * Retrieves a list of vehicles in the account.
     *
     * @return VehiclesData|null
     */
    public function getVehiclesData(): ?VehiclesData
    {
        $TheQuery = null;
        $aHeaders = array(
            'apikey: ' . $this->FConfig->getKamereonAPIKey(),
            'x-gigya-id_token: ' . $this->FConfig->getIDToken(),
        );
        try {
            $sURL = $this->FConfig->getKamereonBaseURL();
            $sURL .= 'commerce/v1/accounts/';
            $sURL .= $this->FConfig->getAccountID();
            $sURL .= '/vehicles';
            $sURL .= '?country=';
            $sURL .=  $this->FConfig->getCountry();
            $q = new Query($sURL);
            $q->setHeaders($aHeaders);
            $res = $q->execute();
            if (is_null($res)) {
                $this->FCURLError = $q->getError();
                $ret = null;
            } else {
                $ret = new VehiclesData($res, $q);
            }
        } catch (\Exception $e) {
            throw new ZoeException($e->getMessage(), $q, null);
        }
        return $ret;
    }
}
