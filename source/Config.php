<?php

/**
 * Configuration handling class.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @see        https://renault-wrd-prod-1-euw1-myrapp-one.s3-eu-west-1.amazonaws.com/configuration/android/config_de_DE.json
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * This class handles all configuration, including login and tokens.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class Config
{
    protected int $FExpirationSeconds = 86700;
    protected string $FAccountID = '';
    protected string $FAuthToken = '';
    protected string $FCountry = '';
    protected string $FGigyaAPIKey = '3_7PLksOyBRkHv126x5WhHb-'
        . '5pqC1qFR8pQjxSeLB6nhAnPERTUlwnYoznHSxwX668';
    protected string $FGigyaServer = 'https://accounts.eu1.gigya.com/';
    protected string $FIDToken = '';
    protected ?\DateTime $FIDTokenExpiry = null;
    protected string $FKamereonAPIKey = 'YjkKtHmGfaceeuExUDKGxrLZGGvtVS0J';
    protected string $FKamereonURL = 'https://api-wired-prod-1-euw1.wrd-aws.com/';
    protected string $FPassword = '';
    protected string $FPersonID = '';
    protected string $FUsername = '';
    protected string $FVIN = '';
    protected ConfigStorage $FStorage;

    /**
     * Constructs a configuration handler.
     *
     * @param ConfigStorage $AStorage The storage to use to read and write to.
     */
    public function __construct(ConfigStorage $AStorage)
    {
        $this->FStorage = $AStorage;
        $this->FUsername = $this->FStorage->readUsername();
        $this->FPassword = $this->FStorage->readPassword();
        $this->FAuthToken = $this->FStorage->readAuthToken();
        $this->FPersonID = $this->FStorage->readPersonID();
        $this->FAccountID = $this->FStorage->readAccountID();
        $this->FVIN = $this->FStorage->readVIN();
        $this->FCountry = $this->FStorage->readCountry('DE');
    }

    /**
     * Returns the Account ID.
     *
     * @return string
     */
    public function getAccountID(): string
    {
        return $this->FAccountID;
    }

    /**
     * Returns the authentication token.
     *
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->FAuthToken;
    }

    /**
     * Returns the API country.
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->FCountry;
    }

    /**
     * Returns the number of seconds a token should be valid.
     *
     * @return int
     */
    public function getExpirationSeconds(): int
    {
        return $this->FExpirationSeconds;
    }

    /**
     * Returns the API key required for the Gigya login service.
     *
     * @return string
     */
    public function getGigyaAPIKey(): string
    {
        return $this->FGigyaAPIKey;
    }

    /**
     * Returns the URL for the Gigya account query
     *
     * @return string
     */
    public function getGigyaAccountURL(): string
    {
        return $this->FGigyaServer . 'accounts.getAccountInfo';
    }
    /**
     * Returns the URL for the Gigya login
     *
     * @return string
     */
    public function getGigyaLoginURL(): string
    {
        return $this->FGigyaServer . 'accounts.login';
    }

    /**
     * Returns the URL for the Gigya Jwt call
     *
     * @return string
     */
    public function getGigyaJwtURL(): string
    {
        return $this->FGigyaServer . 'accounts.getJWT';
    }

    /**
     * Returns the ID token.
     *
     * @return string
     */
    public function getIDToken(): string
    {
        return $this->FIDToken;
    }

    /**
     * Returns the API key for the Kamereon API.
     *
     * @return string
     */
    public function getKamereonAPIKey(): string
    {
        return $this->FKamereonAPIKey;
    }

    /**
     * Returns the base URL for Kamereon API calls.
     *
     * @return string
     */
    public function getKamereonBaseURL(): string
    {
        return $this->FKamereonURL;
    }

    /**
     * Returns the username to be used to access Renault Zoe.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->FUsername;
    }

    /**
     * Returns the password to be used to access Renault Zoe.
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->FPassword;
    }

    /**
     * Returns the Person ID.
     *
     * @return string
     */
    public function getPersonID(): string
    {
        return $this->FPersonID;
    }

    /**
     * Returns the default Vehicle Indentification Number.
     *
     * @return string
     */
    public function getVIN(): string
    {
        return $this->FVIN;
    }

    /**
     * Resets all tokens to allow a fresh login.
     *
     * @return void
     */
    public function resetTokens(): void
    {
        $this->FAccountID = '';
        $this->FPersonID = '';
        $this->FAuthToken = '';
        $this->FIDToken = '';
    }

    /**
     * Set the Account ID.
     *
     * @param string $AnAccountID The Account ID.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setAccountID(string $AnAccountID): Config
    {
        $this->FAccountID = $AnAccountID;
        $this->FStorage->writeAccountID($this->FAccountID);
        return $this;
    }

    /**
     * Sets the authentication token.
     *
     * @param string $AnAuthToken The token to set.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setAuthToken(string $AnAuthToken): Config
    {
        $this->FAuthToken = $AnAuthToken;
        $this->FStorage->writeAuthToken($this->FAuthToken);
        return $this;
    }

    /**
     * Sets the country to use for API calls.
     *
     * @param string $TheCountry The two letter country code.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setCountry(string $TheCountry): Config
    {
        $this->FCountry = $TheCountry;
        return $this;
    }

    /**
     * Set the ID token.
     *
     * @param string $AnIDToken The ID token.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setIDToken(string $AnIDToken): Config
    {
        $this->FIDToken = $AnIDToken;
        return $this;
    }

    /**
     * Set the ID token time, used to check expiry.
     *
     * @param string $AnIDTokenTime The ID token time.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setIDTokenTime(string $AnIDTokenTime): Config
    {
        $dt = new \DateTime($AnIDTokenTime);
        $iSeconds = $this->getExpirationSeconds();
        $dt->add(new \DateInterval("PT{$iSeconds}S"));
        $this->FIDTokenExpiry = $dt;

        return $this;
    }

    /**
     * Sets the Person ID.
     *
     * @param string $APersonID The Person ID.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setPersonID(string $APersonID): Config
    {
        $this->FPersonID = $APersonID;
        $this->FStorage->writePersonID($this->FPersonID);
        return $this;
    }

    /**
     * Sets the username for the Gigya API.
     *
     * @param string $TheUsername The username.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setUsername(string $TheUsername): Config
    {
        $this->FUsername = $TheUsername;
        $this->FStorage->writeUsername($this->FUsername);
        return $this;
    }

    /**
     * Sets the password for the Gigya API.
     *
     * @param string $ThePassword The password.
     *
     * @return Config An instance of itself to chain commands.
     */
    public function setPassword(string $ThePassword): Config
    {
        $this->FPassword = $ThePassword;
        $this->FStorage->writePassword($this->FPassword);
        return $this;
    }

    /**
     * Sets the default Vehicle Identification Number.
     *
     * @param string $TheVIN The VIN.
     *
     * @return Config n instance of itself to chain commands.
     */
    public function setVIN(string $TheVIN): Config
    {
        $this->FVIN = $TheVIN;
        $this->FStorage->writeVIN($this->FVIN);
        return $this;
    }

    /**
     * Asks for required information if running from command line.
     *
     * @return void
     */
    public function requestRequiredCredentialsOnCommandLine(): void
    {
        if (PHP_SAPI === 'cli') {
            if (strlen($this->getUsername()) == 0) {
                echo "\nPlease specify the username: ";
                $input = rtrim(fgets(STDIN));
                $this->setUsername($input);
            }
            if (strlen($this->getPassword()) == 0) {
                echo "\nInput will be visible, ";
                echo "please check your environment.";
                echo "\nPlease specify the password: ";
                $input = rtrim(fgets(STDIN));
                $this->setPassword($input);
            }
        }
        return;
    }
}
