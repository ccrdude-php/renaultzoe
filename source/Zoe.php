<?php

/**
 * Main class to handle Zoe communcation.s
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Main class to handle Zoe communcation.s
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class Zoe
{
    protected Config $FConfig;
    protected Gigya $FGigya;
    protected Kamereon $FKamereon;

    /**
     * Constructs a Renault Zoe handler.
     *
     * @param Config $TheConfig The configuration to be used.
     */
    public function __construct(Config $TheConfig)
    {
        $this->FConfig = $TheConfig;
        $this->FGigya = new Gigya($this->FConfig);
        $this->FKamereon = new Kamereon($this->FConfig);
    }

    /**
     * Retrieves the battery status data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return BatteryStatusData|null
     */
    public function getBattery(?string $TheVIN = null): ?BatteryStatusData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getBatteryStatusData($TheVIN);
    }

    /**
     * Retrieves the cockpit data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return CockpitData|null
     */
    public function getCockpit(?string $TheVIN = null): ?CockpitData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getCockpitData($TheVIN);
    }

    /**
     * Retrieves the charge mode data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return ChargeModeData|null
     */
    public function getChargeMode(?string $TheVIN = null): ?ChargeModeData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getChargeModeData($TheVIN);
    }

    /**
     * Retrieves the charging settings data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return ChargingSettingsData|null
     */
    public function getChargingSettings(
        ?string $TheVIN = null
    ): ?ChargingSettingsData {
        $a = $this->validateLogin();
        return $this->FKamereon->getChargingSettingsData($TheVIN);
    }

    /**
     * Retrieves the location data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return LocationData|null
     */
    public function getLocation(?string $TheVIN = null): ?LocationData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getLocationData($TheVIN);
    }

    /**
     * Retrieves the lock status data from the vehicle.
     *
     * @param string|null $TheVIN Either the Vehicle Indentification Number,
     *                            or null to use the default.
     *
     * @return LockStatusData|null
     */
    public function getLockStatus(?string $TheVIN = null): ?LockStatusData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getLockStatusData($TheVIN);
    }

    /**
     * Retrieves the vehicle list.
     *
     * @return CockpitData|null
     */
    public function getVehicles(): ?VehiclesData
    {
        $a = $this->validateLogin();
        return $this->FKamereon->getVehiclesData();
    }

    /**
     * Checks if all required information is available.
     * If essential information is missing, an exception will be thrown.
     * For temporary things like tokens, they will be queried.
     *
     * @return array Debug information on required information.
     */
    protected function validateLogin(): array
    {
        $aRet = array();
        $this->FConfig->resetTokens();
        if (strlen($this->FConfig->getUsername()) == 0) {
            throw new ZoeException('Configuration has no username set.', null, '');
        }
        if (strlen($this->FConfig->getPassword()) == 0) {
            throw new ZoeException('Configuration has no password set.', null, '');
        }
        $sAuthToken = $this->FConfig->getAuthToken();
        if (strlen($sAuthToken) == 0) {
            $this->FGigya->login();
            $aRet['authToken'] = 'login';
        } else {
            $aRet['authToken'] = "existing: {$sAuthToken}";
        }
        $sPersonID = $this->FConfig->getPersonID();
        if (strlen($sPersonID) == 0) {
            $this->FGigya->retrievePersonID();
            $aRet['PersonID'] = 'login';
        } else {
            $aRet['PersonID'] = "existing: {$sPersonID}";
        }
        $sIDToken = $this->FConfig->getIDToken();
        if (strlen($sIDToken) == 0) {
            try {
                $this->FGigya->retrieveIDToken();
            } catch (GigyaException $e) {
                $iError = $e->getErrorCode();
                if (!is_null($iError)) {
                    if ($iError == 403012) {
                        $this->FConfig->resetTokens();
                        return $this->validateLogin();
                    }
                }
            }
            $aRet['IDToken'] = 'login';
        } else {
            $aRet['IDToken'] = "existing: {$sIDToken}";
        }
        $sAccountID = $this->FConfig->getAccountID();
        if (strlen($sAccountID) == 0) {
            $this->FKamereon->getAccountID();
            $aRet['accountID'] = 'login';
        } else {
            $aRet['accountID'] = "existing: {$sAccountID}";
        }
        return $aRet;
    }
}
