<?php

/**
 * Interface to the Gigya API used by the Renault Zoe interface.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * The class Gigya is responsible for all account related communication
 * with the servers handling Renault Zoe information.
 *
 * It seems to be related to the SAP Customer Data Cloud.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @see        https://help.sap.com/docs/SAP_CUSTOMER_DATA_CLOUD
 * @since      1.0.0
 */
class Gigya
{
    protected Config $FConfig;
    protected string $FCURLError = '';

    /**
     * Constructs a Gigya API handler.
     *
     * @param Config $TheConfig The configuration to be used.
     */
    public function __construct(Config $TheConfig)
    {
        $this->FConfig = $TheConfig;
    }

    /**
     * Returns the error message of any errors occuring inside
     * the CURL library used to communicate with the Gigya server.
     *
     * @return string
     */
    public function getCURLError(): string
    {
        return $this->FCURLError;
    }

    /**
     * The login function connects to the Gigya server using username and
     * password, plus the required API key, which depends on the country,
     * not the user
     *
     * This function saves the returned authentication token within the
     * config structure passed to the constructor.
     *
     * {
     *     "callId": "caffee123456nnnn...",
     *     "errorCode": 0,
     *     "apiVersion": 2,
     *     "statusCode": 200,
     *     "statusReason": "OK",
     *     "time": "2023-04-28T06:12:55.650Z",
     *     "registeredTimestamp": 1654118483,
     *     "UID": "(fixed?)",
     *     "UIDSignature": "....",
     *     "signatureTimestamp": "1682662375",
     *     "created": "2022-06-01T21:21:23.177Z",
     *     "createdTimestamp": 1654118483,
     *     "isActive": true,
     *     "isRegistered": true,
     *     "isVerified": true,
     *     "lastLogin": "2023-04-28T06:12:55.620Z",
     *     "lastLoginTimestamp": 1682662375,
     *     "lastUpdated": "2022-06-01T21:21:42.606Z",
     *     "lastUpdatedTimestamp": 1654118502606,
     *     "loginProvider": "site",
     *     "oldestDataUpdated": "2022-06-01T21:21:23.177Z",
     *     "oldestDataUpdatedTimestamp": 1654118483177,
     *     "profile": {
     *         "email": "renaultzoe@example.com"
     *     },
     *     "registered": "2022-06-01T21:21:23.263Z",
     *     "socialProviders": "site",
     *     "verified": "2022-06-01T21:21:42.606Z",
     *     "verifiedTimestamp": 1654118502606,
     *     "newUser": false,
     *     "sessionInfo": {
     *         "cookieName": "nnnn...",
     *         "cookieValue": "nnnn..."
     *     }
     * }
     *
     * @return bool
     */
    public function login(): bool
    {
        $aPostData = array(
            'ApiKey' => $this->FConfig->getGigyaAPIKey(),
            'loginId' => $this->FConfig->getUsername(),
            'password' => $this->FConfig->getPassword(),
            'sessionExpiration' => 86700,
        );
        $q = new Query($this->FConfig->getGigyaLoginURL());
        $q->setPostData($aPostData);
        $res = $q->execute();
        if (is_null($res)) {
            $this->FCURLError = $q->getError();
            return false;
        } else {
            if (!isset($res['sessionInfo'])) {
                throw new GigyaException(
                    'Server data has no sessionInfo key.',
                    $q,
                    'sessionInfo'
                );
            }
            if (!isset($res['sessionInfo']['cookieValue'])) {
                throw new GigyaException(
                    'Server data has no sessionInfo.cookieValue key.',
                    $q,
                    'sessionInfo'
                );
            }
            $this->FConfig->setAuthToken($res['sessionInfo']['cookieValue']);
            return true;
        }
    }

    /**
     * Using retrieveIDToken(), the id token, required later,
     * can be queried.
     *
     * {
     *     "callId": "caffee1234nnnnn",
     *     "errorCode": 0,
     *     "apiVersion": 2,
     *     "statusCode": 200,
     *     "statusReason": "OK",
     *     "time": "2023-04-28T06:11:44.502Z",
     *     "id_token": "xxxxxxxxxxxxxxxxxx"
     * }
     *
     * @return bool
     */
    public function retrieveIDToken(): bool
    {
        $aPostData = array(
            'login_token' => $this->FConfig->getAuthToken(),
            'ApiKey' => $this->FConfig->getGigyaAPIKey(),
            'fields' => 'data.personId,data.gigyaDataCenter',
            'sessionExpiration' => 86700,
        );
        $q = new Query($this->FConfig->getGigyaJwtURL());
        $q->setPostData($aPostData);
        $res = $q->execute();
        if (is_null($res)) {
            $this->FCURLError = $q->getError();
            return false;
        } else {
            if (!isset($res['time'])) {
                throw new GigyaException(
                    'Server data has no id_token key.',
                    $q,
                    'id_token'
                );
            }
            if (!isset($res['time'])) {
                throw new GigyaException(
                    'Server data has no time key.',
                    $q,
                    'time'
                );
            }
            $this->FConfig->setIDToken($res['id_token']);
            $this->FConfig->setIDTokenTime($res['time']);
            return true;
        }
    }
    /**
     * Using retrievePersonID(), the personId, required later to retrieve accounts,
     * can be queried.
     *
     * This function saves the returned person ID within the
     * config structure passed to the constructor.
     *
     * @return bool
     */
    public function retrievePersonID(): bool
    {
        $aPostData = array(
            'login_token' => $this->FConfig->getAuthToken(),
            'ApiKey' => $this->FConfig->getGigyaAPIKey(),
        );
        $q = new Query($this->FConfig->getGigyaAccountURL());
        $q->setPostData($aPostData);
        $res = $q->execute();
        if (is_null($res)) {
            $this->FCURLError = $q->getError();
            return false;
        } else {
            if (!isset($res['data'])) {
                throw new GigyaException('Server data has no data key.', $q, 'data');
            }
            if (!isset($res['data']['personId'])) {
                throw new GigyaException(
                    'Server data has no data.personId key.',
                    $q,
                    'data.personId'
                );
            }
            $this->FConfig->setPersonID($res['data']['personId']);
            return true;
        }
    }
}
