<?php

/**
 * Configuration storage using a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * This class loads and saves configuration from/to a JSON file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */
class ConfigFileStorage extends ConfigStorage
{
    protected string $FFilename;

    protected ?array $FData = null;

    /**
     * Pass a filename where the configuration should be stored.
     *
     * @param string $AFilename The name of the file to use.
     */
    public function __construct(string $AFilename)
    {
        $this->FFilename = $AFilename;
        if (!file_exists(dirname($this->FFilename))) {
            mkdir(dirname($this->FFilename), 0777, true);
        }
    }

    /**
     * Reads the Account ID to be used by the Kamereon API.
     *
     * @return string
     */
    public function readAccountID(): string
    {
        $this->readFile();
        if (!isset($this->FData['kamereonAccountID'])) {
            return '';
        }
        return $this->FData['kamereonAccountID'];
    }

    /**
     * Reads the authentication token to be used by the Gigya API.
     *
     * @return string
     */
    public function readAuthToken(): string
    {
        $this->readFile();
        if (!isset($this->FData['gigyaAuthToken'])) {
            return '';
        }
        return $this->FData['gigyaAuthToken'];
    }

    /**
     * Reads the country to be used by the API. Endpoints
     * differ based on country, so it's important to set.
     *
     * @param string $ADefault This is the default country to be used if
     *                         none is stored.
     *
     * @return string
     */
    public function readCountry(string $ADefault = 'DE'): string
    {
        $this->readFile();
        if (!isset($this->FData['country'])) {
            return $ADefault;
        }
        return $this->FData['country'];
    }
    /**
     * Reads all stored data from the file specified in the
     * constructor.
     *
     * @return void
     */
    protected function readFile(): void
    {
        if ((is_null($this->FData)) && (file_exists($this->FFilename))) {
            $content = file_get_contents($this->FFilename);
            $this->FData = json_decode($content, true);
        }
    }

    /**
     * Reads the ID token needed to login.
     *
     * @return string
     */
    public function readIDToken(): string
    {
        $this->readFile();
        if (!isset($this->FData['idtoken'])) {
            return '';
        }
        return $this->FData['idtoken'];
    }

    /**
     * Reads the date The ID token will expire.
     *
     * @return string
     */
    public function readIDTokenExpiry(): \DateTime
    {
        $this->readFile();
        if (!isset($this->FData['idtokenexpiry'])) {
            return '';
        }
        return $this->FData['idtokenexpiry'];
    }

    /**
     * Reads the password to be used to authenticate to the Gigya API.
     *
     * @return string
     */
    public function readPassword(): string
    {
        $this->readFile();
        if (!isset($this->FData['password'])) {
            return '';
        }
        return $this->FData['password'];
    }

    /**
     * Reads the Person ID to be used by the Kamereon API.
     *
     * @return string
     */
    public function readPersonID(): string
    {
        $this->readFile();
        if (!isset($this->FData['gigyaPersonID'])) {
            return '';
        }
        return $this->FData['gigyaPersonID'];
    }

    /**
     * Reads the username to be used to authenticate to the Gigya API.
     *
     * @return string
     */
    public function readUsername(): string
    {
        $this->readFile();
        if (!isset($this->FData['username'])) {
            return '';
        }
        return $this->FData['username'];
    }

    /**
     * Reads the Vehicle Identification Number to be used.
     * If not specified, the first vehicle in the account will be used.
     *
     * @return string
     */
    public function readVIN(): string
    {
        $this->readFile();
        if (!isset($this->FData['vin'])) {
            return '';
        }
        return $this->FData['vin'];
    }

    /**
     * Writes the specified Account ID to the config file.
     *
     * @param string $AnID The ID to save.
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writeAccountID(string $AnID): ConfigFileStorage
    {
        $this->FData['kamereonAccountID'] = $AnID;
        $this->writeFile();
        return $this;
    }

    /**
     * Writes the specified Authentication Token to the config file.
     *
     * @param string $AToken The token to save.
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writeAuthToken(string $AToken): ConfigFileStorage
    {
        $this->FData['gigyaAuthToken'] = $AToken;
        $this->writeFile();
        return $this;
    }

    /**
     * Writes the authentication token to be used by the Gigya API.
     *
     * @param string $AVIN The country to be used by the API.
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writeCountry(string $AVIN): ConfigFileStorage
    {
        $this->FData['country'] = $AVIN;
        $this->writeFile();
        return $this;
    }

    /**
     * Saves the configuration into the file passed to the constructor.
     *
     * @return void
     */
    protected function writeFile(): void
    {
        file_put_contents(
            $this->FFilename,
            json_encode($this->FData, JSON_PRETTY_PRINT)
        );
    }

    /**
     * Writes the Person ID to be used by the Kamereon API.
     *
     * @param string $APassword The password.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    public function writePassword(string $APassword): ConfigFileStorage
    {
        $this->FData['password'] = $APassword;
        $this->writeFile();
        return $this;
    }

    /**
     * Writes the Person ID to be used by the Kamereon API.
     *
     * @param string $AnID The Person ID.
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writePersonID(string $AnID): ConfigFileStorage
    {
        $this->FData['gigyaPersonID'] = $AnID;
        $this->writeFile();
        return $this;
    }

    /**
     * Writes the username used for authenticating to the Gigya API.
     *
     * @param string $AUsername The username.
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writeUsername(string $AUsername): ConfigFileStorage
    {
        $this->FData['username'] = $AUsername;
        $this->writeFile();
        return $this;
    }

    /**
     * Writes the Vehicle Identification Number to be used.
     * If not specified, the first vehicle in the account will be used.
     *
     * @param string $AVIN The Vehicle Identification Number .
     *
     * @return ConfigFileStorage An instance of itself to chain commands.
     */
    public function writeVIN(string $AVIN): ConfigFileStorage
    {
        $this->FData['vin'] = $AVIN;
        $this->writeFile();
        return $this;
    }
}
