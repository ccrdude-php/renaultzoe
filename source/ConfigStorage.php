<?php

/**
 * Abstract definitations for a configuration storage file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Class ConfigStorage is an abstract class defining getters and setters
 * to access configuration values.

 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 */
abstract class ConfigStorage
{
    /**
     * Reads the Account ID to be used by the Kamereon API.
     *
     * @return string
     */
    abstract public function readAccountID(): string;
    /**
     * Reads the authentication token to be used by the Gigya API.
     *
     * @return string
     */
    abstract public function readAuthToken(): string;

    /**
     * Reads the country to be used by the API. Endpoints
     * differ based on country, so it's important to set.
     *
     * @param string $ADefault This is the default country to be used if
     *                         none is stored.
     *
     * @return string
     */
    abstract public function readCountry(string $ADefault = 'DE'): string;
    /**
     * Reads the ID token needed to login.
     *
     * @return string
     */
    abstract public function readIDToken(): string;
    /**
     * Reads the date The ID token will expire.
     *
     * @return string
     */
    abstract public function readIDTokenExpiry(): \DateTime;
    /**
     * Reads the password to be used to authenticate to the Gigya API.
     *
     * @return string
     */
    abstract public function readPassword(): string;
    /**
     * Reads the Person ID to be used by the Kamereon API.
     *
     * @return string
     */
    abstract public function readPersonID(): string;
    /**
     * Reads the username to be used to authenticate to the Gigya API.
     *
     * @return string
     */
    abstract public function readUsername(): string;
    /**
     * Reads the Vehicle Identification Number to be used.
     * If not specified, the first vehicle in the account will be used.
     *
     * @return string
     */
    abstract public function readVIN(): string;
    /**
     * Writes the specified Account ID to the config file.
     *
     * @param string $AnID The ID to save.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writeAccountID(string $AnID): ConfigStorage;
    /**
     * Writes the specified Authentication Token to the config file.
     *
     * @param string $AToken The token to save.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writeAuthToken(string $AToken): ConfigStorage;
    /**
     * Writes the authentication token to be used by the Gigya API.
     *
     * @param string $ACountry The country to be used by the API.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writeCountry(string $ACountry): ConfigStorage;
    /**
     * Writes the password used for authenticating to the Gigya API.
     *
     * @param string $APassword The password.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writePassword(string $APassword): ConfigStorage;
    /**
     * Writes the Person ID to be used by the Kamereon API.
     *
     * @param string $AnID The Person ID.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writePersonID(string $AnID): ConfigStorage;
    /**
     * Writes the username used for authenticating to the Gigya API.
     *
     * @param string $AUsername The username.
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writeUsername(string $AUsername): ConfigStorage;
    /**
     * Writes the Vehicle Identification Number to be used.
     * If not specified, the first vehicle in the account will be used.
     *
     * @param string $AVIN The Vehicle Identification Number .
     *
     * @return ConfigStorage An instance of itself to chain commands.
     */
    abstract public function writeVIN(string $AVIN): ConfigStorage;
}
