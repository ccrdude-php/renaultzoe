<?php

namespace RioGrande\RenaultZoe;

class LogWriter
{
    protected string $Path;
    public function __construct(string $APath)
    {
        $this->Path = $APath;
    }

    protected function getLogEntry(): object
    {
        $storage = new ConfigFileStorage($this->Path . '/riogrande-renaultzoe.json');
        $cfg = new Config($storage);
        $zoe = new Zoe($cfg);
        $aCockpit = $zoe->getCockpit();
        $aLocation = $zoe->getLocation();
        $o = (object)array(
            'mileage' => $aCockpit->getTotalMileage(),
            'latitude' => $aLocation->getLatitude(),
            'longitude' => $aLocation->getLongitude(),
            'timestamp' => array(
                'unix' => time(),
                'iso8601' => date('c'),
            ),
        );
        return $o;
    }

    public function addLogLine(): object
    {
        $oEntry = $this->getLogEntry();
        $sFilename = $this->Path . '/mileages-' . date('Ymd') . '.json';
        if (file_exists($sFilename)) {
            $aLog = json_decode(file_get_contents($sFilename), false);
        } else {
            $aLog = [];
        }
        $aLog[] = $oEntry;
        file_put_contents($sFilename, json_encode($aLog, JSON_PRETTY_PRINT));
        return $oEntry;
    }
}
