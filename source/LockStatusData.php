<?php

/**
 * Data class for lock status data.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @since      1.0.0
 */

namespace RioGrande\RenaultZoe;

/**
 * Data class for lock status data.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage RenaultZoe
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/renaultzoe
 * @see        https://renault-api.readthedocs.io/en/latest/endpoints.html#lock-status
 * @since      1.0.0
 */
class LockStatusData extends CustomData
{
    /**
     * Returns the hatch status.
     *
     * @return int
     */
    public function getHatchStatus(): string
    {
        $this->validateDataExists('hatchStatus');
        return $this->FData['data']['attributes']['hatchStatus'];
    }

    /**
     * Returns the overall lock status.
     *
     * @return int
     */
    public function getLockStatus(): string
    {
        $this->validateDataExists('lockStatus');
        return $this->FData['data']['attributes']['lockStatus'];
    }
}
