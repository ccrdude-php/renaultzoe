<?php

namespace RioGrande\RenaultZoe;

require_once('./source/load.php');

header('Content-Type: text/plain');

$lw = new LogWriter(__DIR__ . '/testdata/logs');
$lw->addLogLine();

die('');

$storage = new ConfigFileStorage(__DIR__ . '/riogrande-renaultzoe.json');
$cfg = new Config($storage);
$zoe = new Zoe($cfg);
$aCockpit = $zoe->getCockpit();
$aLocation = $zoe->getLocation();
$o = (object)array(
    'mileage' => $aCockpit->getTotalMileage(),
    'latitude' => $aLocation->getLatitude(),
    'longitude' => $aLocation->getLongitude(),
    'timestamp' => array(
        'unix' => time(),
        'iso8601' => date('c'),
    ),
);
print_r($o);


$sFilename = __DIR__ . '/mileages-' . date('Ymd') . '.json';
if (file_exists($sFilename)) {
    $aLog = json_decode(file_get_contents($sFilename), false);
} else {
    $aLog = [];
}
$aLog[] = $o;

file_put_contents($sFilename, json_encode($aLog, JSON_PRETTY_PRINT));
