---
php-version: 8+
license: mit
---

[[_TOC_]]

# Renault Zoe API

This repository supplies code to access a Renault Zoe using the Gigya and Kamereon 
APIs.

It's not the first implemenation, just a fresh attempt at creating one without 
dependencies. Before I created this, I was using another package that wasn' t
updated any more, and debugging all the external dependencies was too time
consuming, so it was easier to wrap the few REST calls myself.

It is actively used (and developed for) a car sharing like WordPress plugin.

# Todo

- [ ] Read data
  - [x] Cockpit
  - [x] Battery
  - [x] Location
- [ ] Code Tests
  - [x] Exceptions
  - [ ] Gigya
  - [ ] Kamereon

# Other Implementations

## PHP

* [PysX/renault-zoe-api (GitHub)](https://github.com/PysX/renault-zoe-api) (outdated Kamereon API key, fixed locales)
* [SAP/gigya-php-sdk](https://github.com/SAP/gigya-php-sdk) (Just a library to the used Gigya server)

## Python

* [PyZE: Python client for Renault ZE API](https://github.com/jamesremuscat/pyze)
* [hacf-fr/renault-api](https://github.com/hacf-fr/renault-api) plus [documentation](https://renault-api.readthedocs.io/en/latest/reference/renault_api.html)

## Tools accessing a Renault Zoe

* [evcc](https://github.com/evcc-io/evcc)