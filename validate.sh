#!/bin/bash
if [ ! -f ./phpcs.phar ]
then
	curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
fi

echo "Testing..."

php phpcs.phar -v --standard=PSR12 source/
