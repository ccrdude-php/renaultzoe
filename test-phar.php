<?php

require "riogrande-zoe-0.1.phar";

$storage = new \RioGrande\RenaultZoe\ConfigFileStorage(__DIR__ . '/riogrande-renaultzoe.json');
$cfg = new \RioGrande\RenaultZoe\Config($storage);
$cfg->requestRequiredCredentialsOnCommandLine();
$zoe = new \RioGrande\RenaultZoe\Zoe($cfg);

try {
    $oVehicles = $zoe->getVehicles();
    $sTable = $oVehicles->getTableAsPlainText();
    $aCockpit = $zoe->getCockpit();
    $aLocation = $zoe->getLocation();
    $aBattery = $zoe->getBattery();
    $sLocation = sprintf("%.4f, %f.4", $aLocation->getLatitude(), $aLocation->getLongitude());
    echo <<<FOOBAR

    {$sTable}

    Total Mileage: {$aCockpit->getTotalMileage()} km
    Location:      {$sLocation}
    Battery:       {$aBattery->getBatteryLevel()} %


    FOOBAR;
    // echo $zoe->getLockStatus()->dumpDataJSON();
} catch (\RioGrande\RenaultZoe\ZoeException $e) {
    echo "\n\nZoeException: {$e->getMessage()}\n";
    $q = $e->getQuery();
    if (!is_null($q)) {
        echo "Query URL: {$q->getURL()}\n";
        print_r($q->getDebugOutput());
    }
}
