#!/bin/bash
if [ ! -f ./tests/phpunit-10.phar ]
then
        curl -L https://phar.phpunit.de/phpunit-10.phar --output ./tests/phpunit-10.phar
fi

php tests/phpunit-10.phar ./tests  --colors auto --testdox
