<?php

try {
    $sFilename = 'riogrande-zoe-0.1.phar';
    if (file_exists($sFilename)) {
        unlink($sFilename);
    }
    if (file_exists($sFilename . '.gz')) {
        unlink($sFilename . '.gz');
    }

    $phar = new Phar($sFilename);
    $phar->startBuffering();
    $defaultStub = $phar->createDefaultStub('load.php');
    $phar->buildFromDirectory(__DIR__ . '/source');
    $phar->setStub($defaultStub);
    $phar->stopBuffering();

    // plus - compressing it into gzip
    $phar->compressFiles(Phar::GZ);

    # Make the file executable
    chmod(__DIR__ . "/{$sFilename}", 0770);

    echo "$sFilename successfully created" . PHP_EOL;
} catch (Exception $e) {
    echo $e->getMessage();
}
